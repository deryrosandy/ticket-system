/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : localhost:3306
 Source Schema         : ticket_system_db

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 21/02/2021 13:42:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for article_categories
-- ----------------------------
DROP TABLE IF EXISTS `article_categories`;
CREATE TABLE `article_categories`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `slug` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `category_description` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `category_icon` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ordering` int(11) NOT NULL,
  `created_on` datetime(0) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_on` datetime(0) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of article_categories
-- ----------------------------

-- ----------------------------
-- Table structure for article_votes
-- ----------------------------
DROP TABLE IF EXISTS `article_votes`;
CREATE TABLE `article_votes`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1-Userfull, 0-Not Userfull',
  `updated_on` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of article_votes
-- ----------------------------

-- ----------------------------
-- Table structure for articles
-- ----------------------------
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `article_title` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `slug` varchar(600) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `article_excerpt` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `article_description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1-Published, 0-Not Published',
  `ordering` int(11) NOT NULL,
  `created_on` datetime(0) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_on` datetime(0) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of articles
-- ----------------------------

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_type` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `config_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `config_value` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `updated_on` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES (1, 'site', 'site_name', 'XTicket System', '2021-02-21 05:44:45');
INSERT INTO `config` VALUES (2, 'site', 'site_email', 'deryrosandy@gmail.com', '2021-02-21 05:44:45');
INSERT INTO `config` VALUES (3, 'site', 'site_phone', '085710011524', '2021-02-21 05:44:45');
INSERT INTO `config` VALUES (4, 'site', 'site_logo', 'b2052c8e7660ff762558ca5dfde21c79.png', '2021-02-21 05:44:45');
INSERT INTO `config` VALUES (5, 'site', 'site_favicon', 'c443956c76fe972aa5bb26cd3da36074.png', '2019-11-13 13:06:12');
INSERT INTO `config` VALUES (6, 'social', 'facebook', 'https://www.facebook.com/', '2020-01-17 08:51:24');
INSERT INTO `config` VALUES (7, 'social', 'twitter', 'https://www.facebook.com/', '2020-01-17 08:51:24');
INSERT INTO `config` VALUES (8, 'social', 'instagram', 'https://www.instagram.com/', '2020-01-17 08:51:24');
INSERT INTO `config` VALUES (9, 'social', 'linkedin', 'https://www.linkedin.com/', '2020-01-17 08:51:24');
INSERT INTO `config` VALUES (10, 'social', 'google_plus', 'https://aboutme.google.com', '2020-01-17 08:51:24');
INSERT INTO `config` VALUES (11, 'social', 'youtube', 'https://www.youtube.com/', '2020-01-17 08:51:24');
INSERT INTO `config` VALUES (12, 'social', 'github', 'https://github.com/', '2020-01-17 08:51:24');
INSERT INTO `config` VALUES (13, 'seo', 'meta_title', 'Home | Xticket System', '2021-02-21 05:45:46');
INSERT INTO `config` VALUES (14, 'seo', 'meta_description', 'Support Tickets Sytem, For Support Tickets and Knowledge Bases', '2021-02-21 05:45:46');
INSERT INTO `config` VALUES (15, 'seo', 'meta_keywords', 'Support System, Ticket System, Knowledge Base', '2021-02-21 05:45:46');
INSERT INTO `config` VALUES (16, 'seo', 'google_analytics', '', '2021-02-21 05:45:46');
INSERT INTO `config` VALUES (17, 'app', 'email_notify_new_ticket', '1', '2020-01-17 11:33:06');
INSERT INTO `config` VALUES (18, 'app', 'email_notify_assign_ticket', '1', '2020-01-17 11:33:06');
INSERT INTO `config` VALUES (19, 'app', 'send_password_created_new_user', '1', '2020-01-17 11:33:06');
INSERT INTO `config` VALUES (20, 'app', 'allow_guest_ticket_submission', '1', '2020-01-17 11:33:06');
INSERT INTO `config` VALUES (21, 'app', 'email_notify_reply_ticket', '1', '2020-02-18 00:00:00');
INSERT INTO `config` VALUES (22, 'email', 'mail_from_email', 'deryrosandy@gmail.com', '2021-02-21 05:46:41');
INSERT INTO `config` VALUES (23, 'email', 'mail_host', 'mail.google.com', '2021-02-21 05:46:41');
INSERT INTO `config` VALUES (24, 'email', 'mail_username', '_mailer@google.com', '2021-02-21 05:46:41');
INSERT INTO `config` VALUES (25, 'email', 'mail_port', '465', '2021-02-21 05:46:41');
INSERT INTO `config` VALUES (26, 'email', 'mail_password', '1234567890', '2021-02-21 05:46:41');
INSERT INTO `config` VALUES (27, 'email', 'mail_encryption', 'SSL', '2021-02-21 05:46:41');
INSERT INTO `config` VALUES (28, 'email', 'mail_driver', 'MAIL', '2021-02-21 05:46:41');
INSERT INTO `config` VALUES (29, 'email', 'mail_from_title', 'Xticket System', '2021-02-21 05:46:41');

-- ----------------------------
-- Table structure for email_templates
-- ----------------------------
DROP TABLE IF EXISTS `email_templates`;
CREATE TABLE `email_templates`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `slug` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `template_variables` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `template_content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of email_templates
-- ----------------------------
INSERT INTO `email_templates` VALUES (1, 'Forgot Password', 'forgot_password', '{fullname},{reset_link}', '<p>Hello {fullname}!</p><p>Follow the below link to Reset Your Password. </p><p>{reset_link}</p>', 1, '2020-04-11 07:41:25');
INSERT INTO `email_templates` VALUES (2, 'Reset Password', 'reset_password', '{fullname}', '<p>Hello {fullname}!</p><p>Your Password is successfully Reset.', 1, '2020-03-13 00:00:00');
INSERT INTO `email_templates` VALUES (3, 'Registration', 'registration', '{fullname},{activation_code},{activation_button}', '<p>Hello {fullname}!</p><p>You are successfully registered. Your activation code is. </p><h4>{activation_code}</h4>\r\n<p>{activation_button}</p>', 1, '2020-04-11 08:35:38');
INSERT INTO `email_templates` VALUES (4, 'New Ticket', 'new_ticket', '{sitename},{fullname},{ticket_title},{ticket_description}', '<p>Hello {sitename}!</p><p>A new ticket generated by {fullname}. </p><h4>{ticket_title}</h4><p>{ticket_description}</p>', 1, '2020-04-11 08:43:41');
INSERT INTO `email_templates` VALUES (5, 'Contact Form', 'site_contact', '{sitename},{fullname},{subject},{email},{message}', '<p>Hello {sitename}!</p><p>You have message from {fullname},</p><p>Subject : {subject},</p><p>Email : <a href=\"mailto:{email}\">{email}</a><br>Message: {message}<br></p>', 1, '2020-04-11 08:50:14');
INSERT INTO `email_templates` VALUES (6, 'Reply to Ticket', 'reply_ticket', '{fullname},{ticket_title},{reply_content}', '<p>Hello {fullname}!</p><p>You have reply for a Ticket. </p><h4>{ticket_title}</h4><p>{reply_content}</p>', 1, '2020-04-11 08:58:05');
INSERT INTO `email_templates` VALUES (7, 'Ticket Assigned', 'assign_ticket', '{fullname},{ticket_title},{ticket_description}', '<p>Hello {fullname}!</p><p>You have assigned with a Ticket. </p><h4>{ticket_title}</h4><p>{ticket_description}</p>', 1, '2020-04-08 12:38:53');
INSERT INTO `email_templates` VALUES (8, 'New User', 'user_creation', '{fullname},{sitename},{password}', '<p>Hello {fullname}!</p><p>You have registered in {sitename}</p><p>You can access the application to below password:</p><h3>{password}</h3>', 1, '2020-04-11 09:18:29');

-- ----------------------------
-- Table structure for faq_categories
-- ----------------------------
DROP TABLE IF EXISTS `faq_categories`;
CREATE TABLE `faq_categories`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `slug` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `category_description` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ordering` int(11) NOT NULL,
  `created_on` datetime(0) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_on` datetime(0) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of faq_categories
-- ----------------------------

-- ----------------------------
-- Table structure for faqs
-- ----------------------------
DROP TABLE IF EXISTS `faqs`;
CREATE TABLE `faqs`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `faq_title` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `slug` varchar(550) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `faq_description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1-Active, 0-Inactive',
  `ordering` int(11) NOT NULL,
  `created_on` datetime(0) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_on` datetime(0) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of faqs
-- ----------------------------

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_name` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `permission_slug` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `permission_info` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `updated_on` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 52 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES (1, 'List Articles', 'list_articles', 'List all Articles', '2019-04-12 00:00:00');
INSERT INTO `permissions` VALUES (2, 'View Article', 'view_article', 'View Single Article', '2019-04-12 00:00:00');
INSERT INTO `permissions` VALUES (3, 'Add Article', 'add_article', 'Create New Articles', '2019-04-12 00:00:00');
INSERT INTO `permissions` VALUES (4, 'Edit Article', 'edit_article', 'Edit and Update Articles', '2019-04-12 00:00:00');
INSERT INTO `permissions` VALUES (5, 'Publishing Articles', 'publishing_article', 'Publish or Unpublish Article from Site', '2019-04-12 00:00:00');
INSERT INTO `permissions` VALUES (6, 'Delete Article', 'delete_article', 'Delete Article from Site', '2019-04-12 00:00:00');
INSERT INTO `permissions` VALUES (7, 'Article Ordering', 'article_ordering', 'Update Article Orders', '2019-04-12 00:00:00');
INSERT INTO `permissions` VALUES (8, 'List Article Categories', 'list_article_categories', 'List All Article Categories', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (9, 'View Article Category', 'view_article_category', 'View Single Article Category', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (10, 'Add Article Category', 'add_article_category', 'Add New Article Category', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (11, 'Edit Article Category', 'edit_article_category', 'Edit and Update Article Category', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (12, 'Delete Article Category', 'delete_article_category', 'Delete Article Category From Site', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (13, 'Article Category Ordering', 'article_category_ordering', 'Update Article Category Orders', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (14, 'List Tickets', 'list_tickets', 'List All Tickets', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (15, 'View and Reply Ticket', 'view_reply_ticket', 'View and Reply to Ticket', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (16, 'Assign Ticket', 'assign_ticket', 'Assing Ticket to User', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (17, 'Delete Ticket', 'delete_ticket', 'Delete Ticket from Site', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (18, 'Ticket Completion', 'ticket_completion', 'Mark Ticket as Completed or Not Completed', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (19, 'List Ticket Categories', 'list_ticket_categories', 'List All Ticket Categories', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (20, 'View Ticket Category', 'view_ticket_category', 'View Single Ticket Category', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (21, 'Add Ticket Category', 'add_ticket_category', 'Add New Ticket Category', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (22, 'Edit Ticket Category', 'edit_ticket_category', 'Edit and Update Ticket Category', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (23, 'Delete Ticket Category', 'delete_ticket_category', 'Delete Ticket Category From Site', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (24, 'Ticket Category Ordering', 'ticket_category_ordering', 'Update Ticket Category Orders', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (25, 'List FAQs', 'list_faqs', 'List All FAQs', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (26, 'View FAQ', 'view_faq', 'View Single FAQ', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (27, 'Add FAQ', 'add_faq', 'Add New FAQ', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (28, 'Edit FAQ', 'edit_faq', 'Edit and Update FAQ', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (29, 'FAQ Publishing', 'faq_publishing', 'Publish or Unpublish FAQ', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (30, 'Delete FAQ', 'delete_faq', 'Delete FAQ From Site', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (31, 'FAQ Ordering', 'faq_ordering', 'Update Order of FAQ', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (32, 'List FAQ Categories', 'list_faq_categories', 'List All FAQ Categories', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (33, 'View FAQ Category', 'view_faq_category', 'View Single FAQ Category', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (34, 'Add FAQ Category', 'add_faq_category', 'Add New FAQ Category', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (35, 'Edit FAQ Category', 'edit_faq_category', 'Edit and Update FAQ Category', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (36, 'Delete FAQ Category', 'delete_faq_category', 'Delete FAQ Category From Site', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (37, 'FAQ Category Ordering', 'faq_category_ordering', 'Update FAQ Category Orders', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (38, 'List Users', 'list_users', 'List All Users', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (39, 'View User', 'view_user', 'View a Single User', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (40, 'Add User', 'add_user', 'Add New User', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (41, 'Edit User', 'edit_user', 'Edit and Update User', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (42, 'Delete User', 'delete_user', 'Delete User From Site', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (43, 'User Blocking', 'user_blocking', 'Blocking and Unblocking of User', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (44, 'User Permissions', 'user_permissions', 'User-Based Permission', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (45, 'Site Settings', 'site_settings', 'Update Site Information', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (46, 'Social Media Settings', 'social_media_settings', 'Update Social Media Links', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (47, 'SEO Settings', 'seo_settings', 'Update Search Enngine Optimization Settings', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (48, 'Role Permissions', 'role_permissions', 'Role Based Permissions', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (49, 'App Settings', 'app_settings', 'Basic App Settings', '2019-05-13 00:00:00');
INSERT INTO `permissions` VALUES (50, 'Email Settings', 'email_settings', 'Manage Email Settings', '2020-03-13 00:00:00');
INSERT INTO `permissions` VALUES (51, 'Email Templates', 'email_templates', 'Manage Email Templates', '2020-03-13 00:00:00');

-- ----------------------------
-- Table structure for role_permissions
-- ----------------------------
DROP TABLE IF EXISTS `role_permissions`;
CREATE TABLE `role_permissions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `is_permitted` tinyint(4) NOT NULL COMMENT '0-Not Permitted, 1 - Permitted',
  `updated_on` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 103 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of role_permissions
-- ----------------------------
INSERT INTO `role_permissions` VALUES (1, 2, 1, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (2, 2, 2, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (3, 2, 3, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (4, 2, 4, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (5, 2, 5, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (6, 2, 6, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (7, 2, 7, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (8, 2, 8, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (9, 2, 9, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (10, 2, 10, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (11, 2, 11, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (12, 2, 12, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (13, 2, 13, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (14, 2, 14, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (15, 2, 15, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (16, 2, 16, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (17, 2, 17, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (18, 2, 18, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (19, 2, 19, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (20, 2, 20, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (21, 2, 21, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (22, 2, 22, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (23, 2, 23, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (24, 2, 24, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (25, 2, 25, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (26, 2, 26, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (27, 2, 27, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (28, 2, 28, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (29, 2, 29, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (30, 2, 30, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (31, 2, 31, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (32, 2, 32, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (33, 2, 33, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (34, 2, 34, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (35, 2, 35, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (36, 2, 36, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (37, 2, 37, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (38, 2, 38, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (39, 2, 39, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (40, 2, 40, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (41, 2, 41, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (42, 2, 42, 1, '2019-05-14 05:04:41');
INSERT INTO `role_permissions` VALUES (43, 2, 43, 1, '2019-05-14 06:06:55');
INSERT INTO `role_permissions` VALUES (44, 2, 44, 0, '2019-05-14 06:06:46');
INSERT INTO `role_permissions` VALUES (45, 2, 45, 0, '2019-05-14 06:06:31');
INSERT INTO `role_permissions` VALUES (46, 2, 46, 0, '2019-05-14 06:06:33');
INSERT INTO `role_permissions` VALUES (47, 2, 47, 0, '2019-05-14 06:06:36');
INSERT INTO `role_permissions` VALUES (48, 2, 48, 0, '2019-05-14 06:06:38');
INSERT INTO `role_permissions` VALUES (49, 2, 49, 0, '2019-05-14 06:06:40');
INSERT INTO `role_permissions` VALUES (50, 3, 1, 1, '2019-05-14 05:05:00');
INSERT INTO `role_permissions` VALUES (51, 3, 2, 1, '2019-05-14 05:05:00');
INSERT INTO `role_permissions` VALUES (52, 3, 3, 0, '2019-05-14 06:03:14');
INSERT INTO `role_permissions` VALUES (53, 3, 4, 0, '2019-05-14 06:03:15');
INSERT INTO `role_permissions` VALUES (54, 3, 5, 0, '2019-05-14 06:03:18');
INSERT INTO `role_permissions` VALUES (55, 3, 6, 0, '2019-05-14 05:43:35');
INSERT INTO `role_permissions` VALUES (56, 3, 7, 0, '2019-05-14 06:03:27');
INSERT INTO `role_permissions` VALUES (57, 3, 8, 1, '2019-05-14 05:05:00');
INSERT INTO `role_permissions` VALUES (58, 3, 9, 1, '2019-05-14 05:05:00');
INSERT INTO `role_permissions` VALUES (59, 3, 10, 0, '2019-05-14 06:03:39');
INSERT INTO `role_permissions` VALUES (60, 3, 11, 0, '2019-05-14 06:03:44');
INSERT INTO `role_permissions` VALUES (61, 3, 12, 0, '2019-05-14 06:03:46');
INSERT INTO `role_permissions` VALUES (62, 3, 13, 0, '2019-05-14 06:03:48');
INSERT INTO `role_permissions` VALUES (63, 3, 14, 1, '2019-05-14 05:05:00');
INSERT INTO `role_permissions` VALUES (64, 3, 15, 1, '2019-05-14 05:05:00');
INSERT INTO `role_permissions` VALUES (65, 3, 16, 0, '2019-05-14 06:04:06');
INSERT INTO `role_permissions` VALUES (66, 3, 17, 0, '2019-05-14 06:04:13');
INSERT INTO `role_permissions` VALUES (67, 3, 18, 1, '2019-05-14 05:05:00');
INSERT INTO `role_permissions` VALUES (68, 3, 19, 1, '2019-05-14 05:05:00');
INSERT INTO `role_permissions` VALUES (69, 3, 20, 1, '2019-05-14 05:05:00');
INSERT INTO `role_permissions` VALUES (70, 3, 21, 0, '2019-05-14 06:04:35');
INSERT INTO `role_permissions` VALUES (71, 3, 22, 0, '2019-05-14 06:04:36');
INSERT INTO `role_permissions` VALUES (72, 3, 23, 0, '2019-05-14 06:04:38');
INSERT INTO `role_permissions` VALUES (73, 3, 24, 0, '2019-05-14 06:04:40');
INSERT INTO `role_permissions` VALUES (74, 3, 25, 1, '2019-05-14 05:05:00');
INSERT INTO `role_permissions` VALUES (75, 3, 26, 1, '2019-05-14 05:05:00');
INSERT INTO `role_permissions` VALUES (76, 3, 27, 0, '2019-05-14 06:05:09');
INSERT INTO `role_permissions` VALUES (77, 3, 28, 0, '2019-05-14 06:05:10');
INSERT INTO `role_permissions` VALUES (78, 3, 29, 0, '2019-05-14 06:05:11');
INSERT INTO `role_permissions` VALUES (79, 3, 30, 0, '2019-05-14 06:05:12');
INSERT INTO `role_permissions` VALUES (80, 3, 31, 0, '2019-05-14 06:05:22');
INSERT INTO `role_permissions` VALUES (81, 3, 32, 1, '2019-05-14 05:05:00');
INSERT INTO `role_permissions` VALUES (82, 3, 33, 1, '2019-05-14 05:05:00');
INSERT INTO `role_permissions` VALUES (83, 3, 34, 0, '2019-05-14 06:05:28');
INSERT INTO `role_permissions` VALUES (84, 3, 35, 0, '2019-05-14 06:05:29');
INSERT INTO `role_permissions` VALUES (85, 3, 36, 0, '2019-05-14 06:05:32');
INSERT INTO `role_permissions` VALUES (86, 3, 37, 0, '2019-05-14 06:05:33');
INSERT INTO `role_permissions` VALUES (87, 3, 38, 0, '2019-05-14 06:05:53');
INSERT INTO `role_permissions` VALUES (88, 3, 39, 0, '2019-05-14 06:05:55');
INSERT INTO `role_permissions` VALUES (89, 3, 40, 0, '2019-05-14 06:05:56');
INSERT INTO `role_permissions` VALUES (90, 3, 41, 0, '2019-05-14 06:06:06');
INSERT INTO `role_permissions` VALUES (91, 3, 42, 0, '2019-05-14 06:06:07');
INSERT INTO `role_permissions` VALUES (92, 3, 43, 0, '2019-05-14 06:06:11');
INSERT INTO `role_permissions` VALUES (93, 3, 44, 0, '2019-05-14 06:06:12');
INSERT INTO `role_permissions` VALUES (94, 3, 45, 0, '2019-05-14 06:06:30');
INSERT INTO `role_permissions` VALUES (95, 3, 46, 0, '2019-05-14 06:06:32');
INSERT INTO `role_permissions` VALUES (96, 3, 47, 0, '2019-05-14 06:06:34');
INSERT INTO `role_permissions` VALUES (97, 3, 48, 0, '2019-05-14 06:06:37');
INSERT INTO `role_permissions` VALUES (98, 3, 49, 0, '2019-05-14 06:06:39');
INSERT INTO `role_permissions` VALUES (99, 2, 50, 0, '2020-04-08 13:20:37');
INSERT INTO `role_permissions` VALUES (100, 2, 51, 0, '2020-04-11 13:20:37');
INSERT INTO `role_permissions` VALUES (101, 3, 50, 0, '2020-04-11 13:22:11');
INSERT INTO `role_permissions` VALUES (102, 3, 51, 0, '2020-04-11 13:22:11');

-- ----------------------------
-- Table structure for ticket_categories
-- ----------------------------
DROP TABLE IF EXISTS `ticket_categories`;
CREATE TABLE `ticket_categories`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `slug` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `category_description` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ordering` int(11) NOT NULL,
  `created_on` datetime(0) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_on` datetime(0) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ticket_categories
-- ----------------------------

-- ----------------------------
-- Table structure for ticket_replies
-- ----------------------------
DROP TABLE IF EXISTS `ticket_replies`;
CREATE TABLE `ticket_replies`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(11) NOT NULL,
  `reply_content` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `reply_file` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime(0) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ticket_replies
-- ----------------------------

-- ----------------------------
-- Table structure for tickets
-- ----------------------------
DROP TABLE IF EXISTS `tickets`;
CREATE TABLE `tickets`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `client_name` varchar(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'Uses Only When Guest Ticket',
  `client_email` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'Uses Only When Guest Ticket',
  `ticket_title` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ticket_description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ticket_file` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `assigned_to` int(11) NOT NULL DEFAULT 0,
  `priority` enum('L','M','H','U') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'L-Low, M-Medium, H-High, U-Urgent',
  `status` tinyint(4) NOT NULL COMMENT '0-New,1-In Progress, 2-Closed',
  `created_on` datetime(0) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_on` datetime(0) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tickets
-- ----------------------------

-- ----------------------------
-- Table structure for user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `user_permissions`;
CREATE TABLE `user_permissions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `is_permitted` tinyint(4) NOT NULL COMMENT '0-Not Permitted, 1 - Permitted',
  `updated_on` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_id` int(11) NOT NULL,
  `full_name` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `mobile` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `activation_code` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `reset_token` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `login_agent` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `last_login` datetime(0) NOT NULL,
  `login_ip` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `profile_image` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-Inactive,1-Active,2-Blocked',
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_on` datetime(0) NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_on` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 1, 'Administrator', 'admin@admin.com', '085710011524', '$2y$10$THTRKIgo4Kc4n29hSINXd.AjdtCG8cbMBt9h1aYzzEx.49cYjdWDa', '', '', 'Windows 10-Chrome 88.0.4324.182', '2021-02-21 05:53:20', '::1', 'uploads/profile/5e21a35661a64.png', 1, 1, '2019-04-10 00:00:00', 1, '2021-02-21 05:47:40');
INSERT INTO `users` VALUES (2, 4, 'user', 'user@user.com', '', '$2y$10$LbRs/uV2IGVVMBT20/eIW.Zrz4yNB/JjNMv6X68g96xZec1A2kIVK', '', '', 'Windows 10-Chrome 88.0.4324.182', '2021-02-21 05:51:25', '::1', '', 1, 1, '2021-01-28 16:40:08', 1, '2021-01-28 16:40:08');
INSERT INTO `users` VALUES (3, 2, 'manager', 'manager@manager.com', '', '$2y$10$OTnj2zUkTtJnSYYqPv7O0.EhoviO.bWkQerGff26OZ5dtlwpYhOuG', '', '', 'Windows 10-Chrome 88.0.4324.182', '2021-02-21 06:00:30', '::1', '', 1, 1, '2021-02-21 05:59:12', 1, '2021-02-21 05:59:12');
INSERT INTO `users` VALUES (4, 3, 'Support Agent', 'support@support.com', '', '$2y$10$ugDXLjlUUjmbbQujFilKYeEJI24Ct89GBVXpfE5u0Zo/OUi8wjqQG', '', '', '', '0000-00-00 00:00:00', '', '', 1, 1, '2021-02-21 05:59:56', 1, '2021-02-21 05:59:56');

-- ----------------------------
-- Table structure for users_roles
-- ----------------------------
DROP TABLE IF EXISTS `users_roles`;
CREATE TABLE `users_roles`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `role_slug` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `updated_on` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users_roles
-- ----------------------------
INSERT INTO `users_roles` VALUES (1, 'Administrator', 'admin', '2019-04-10 00:00:00');
INSERT INTO `users_roles` VALUES (2, 'Support Manager', 'manager', '2019-04-10 00:00:00');
INSERT INTO `users_roles` VALUES (3, 'Support Agent', 'agent', '2019-04-10 00:00:00');
INSERT INTO `users_roles` VALUES (4, 'Customer', 'customer', '2019-04-16 00:00:00');

SET FOREIGN_KEY_CHECKS = 1;
